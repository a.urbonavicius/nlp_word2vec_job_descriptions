# NLP_word2vec_job_descriptions

  
Using data from jobs.ch will explore word2vec algorithm:

- Create a corpus from job descriptions
- Train the model
- Visualise embeddings using dimensionality reduction
- Find the most similar/closest words for a given word